public class IO {
	public byte input() {
		java.util.Scanner scnnr = new java.util.Scanner(System.in);
		System.out.print("please enter a number: ");
		try {
			byte retval = scnnr.nextByte();
			if (retval < 0 || retval >123) {
				throw new java.util.InputMismatchException();
			}
			scnnr.close();
			return retval;
		} catch (java.util.InputMismatchException e) {
			System.out.println("The number you entered can not be parsed. Maybe it is to big or negative. Please enter a new one.");
			return input(); //recursive function calling
		}
	}	

}
