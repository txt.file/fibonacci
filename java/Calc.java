
public class Calc {
	public long getFibonacci(byte n) {
		long[] fArray =  new long[Byte.MAX_VALUE];
		fArray[0] = 0;
		fArray[1] = 1;
		for (byte i = 2; i <= n; i++) {
			fArray[i] = (fArray[i-1])+(fArray[i-2]);
		}
		return fArray[n];
	}

}
