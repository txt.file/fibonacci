/**
 * @author Matthias Fritzsche
 */

/**
 * @author Matthias Fritzsche
 *
 */
public class Fibonacci {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// input
		IO io = new IO();
		byte index = io.input();
		
		// calculation
		Calc clc = new Calc();
		long value = clc.getFibonacci(index);
		
		// output
		System.out.println(value);
	}
}

