#!/bin/sh
# Main.tcl \
exec tclsh "$0" ${1+"$@"}

proc calc {number} {
	set fib(1) 0
	set fib(2) 1
	for {set i 3} {$i <= $number } {incr i} {
		set x $fib([ expr $i - 2 ])
		set y $fib([ expr $i - 1 ])
		set fib($i) [ expr $x + $y ]
	}
	return $fib($number)
}

set index $argv
set ret [calc $index]
puts "fibonnacci number $index is $ret"
